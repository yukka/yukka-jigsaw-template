@extends('_layouts.master')

@section('body')
  <div class="container text-center">
      <h1>Ruud Zonnenberg</h1>
      <hr>
      <a href="https://gitlab.com/ruudz" target="_blank" rel="noopener"><i class="fab fa-gitlab fa-3x"></i></a>
      <a href="https://github.com/ruudz" target="_blank" rel="noopener"><i class="fab fa-github fa-3x"></i></a>
      {{-- <a href=""><i class="fab fa-twitter fa-3x"></i></a> --}}
      <a href="https://www.linkedin.com/in/ruud-zonnenberg/" target="_blank" rel="noopener"><i class="fab fa-linkedin fa-3x"></i></a>
      <a href="https://www.facebook.com/rzonnenberg1" target="_blank" rel="noopener"><i class="fab fa-facebook fa-3x"></i></a><br>
      {{-- <p class="lead">A template which includes Bootstrap 4, Font awesome, Animate.css, Vue, SASS / partial structures</p> --}}
      <sub class="m-0 text-center text-white">Copyright &copy; Ruud Zonnenberg</sub>
  </div>
  @foreach ($sections as $section)
      <section id="{{ str_slug($section->title) }}">
          <div class="container">
              <div class="row">
                  <div class="col-lg-8 mx-auto">
                      <h1>{{ $section->title }}</h1>
                      {!! $section !!}
                  </div>
              </div>
          </div>
      </section>
  @endforeach
@endsection
